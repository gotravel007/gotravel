<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'bootstrap/css/bootstrap.css',
        'css/ele-style.css',
        'css/style.css',
        'css/responsive.css',
        'font-awesome/css/font-awesome.min.css',
        'js/owl-carousel/owl.carousel.css',
        'js/dist/css/bootstrap-select.css',
    ];
    public $js = [
        //'js/moment.js',
        'js/dist/js/bootstrap-select.js',
        'js/owl-carousel/owl.carousel.min.js',
        'js/internal.js',
        'js/switcher.js',
        'bootstrap/js/bootstrap.min.js',
        'js/jquery.2.1.1.min.js',
    ];
    /*public $ttf = [
        'fonts/ElegantIcons.ttf',
    ];

    public $woff = [
        'fonts/ElegantIcons.woff',
    ];*/

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
