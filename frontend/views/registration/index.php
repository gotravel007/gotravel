<?php
/* @var $this yii\web\View */
?>
<!--<h1>registration/index</h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>-->




<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>

<div class="bread-crumb">
    <div class="container">
        <h2>Signup</h2>
        <ul class="list-inline">
            
            <li><?php
            $this->title = 'Signup';
            $this->params['breadcrumbs'][] = $this->title;
            ?></li>
        </ul>
    </div>
</div>

<div class="site-signup">
    
    <div class="col-sm-6">
    <div class="leftside"></div></div>
    <div class="container">
        <div id="content" class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="loginto">
                        <h2>Signup New Account</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.</p>
                        <div class="donot">Don't have account -
                        <a href="register.html">Login now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">
                    <div class="loginnow">
                        <div class="places">
                            <h1>Signup now</h1>
                            <p>please signup to new account</p>
                            <hr>
                        </div>
                        
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                

                <?= $form->field($model, 'firstname')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'lastname')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'address')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'status')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'usertype')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'contactno')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Registration', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        
                    </div>
                </div>          
            </div>
        </div>
    </div>
</div>
