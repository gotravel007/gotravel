<!--<?php
/* @var $this yii\web\View */
?>
<h1>/shopcart/index</h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>
-->

<!-- breadcrumb start here -->
<div class="bread-crumb">
	<div class="container">
		<h2>Shopping Cart</h2>
		<ul class="list-inline">
			<li><a href="index.html">home</a></li>
			<li><a href="shop.html">MY ACCOUNT</a></li>
			<li><a href="shoppingcart.html">Shopping Cart</a></li>
		</ul>
	</div>
</div>
<!-- breadcrumb end here -->

<!-- checkout start here -->
<div class="mycart">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h4>Shopping Cart Summary <span>Items 3</span></h4>
				<form method="post" enctype="multipart/form-data">
					<div class="table-responsive">
						<table class="table tabl1 table-bordered">
							<thead>
								<tr>
									<td class="text-center">Products</td>
									<td class="text-center">Price</td>
									<td class="text-center">Qty.</td>
									<td class="text-center">Total Price</td>
									<td class="text-center">Delete</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">
										<a href="#">
											<img src="images/shop/thumb1.jpg" class="img-responsive" alt="thumb" title="thumb" />
											<div class="name">Travel Bag
												<div class="rating">
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star-half-o" aria-hidden="true"></i>
												</div>
												<p>Color : <span>Black</span></p>
											</div>
										</a>
									</td>
									<td class="text-center">$ 35.00</td>
									<td class="text-center">
										<p class="qtypara">
											<span id="minus2" class="minus"><i class="fa fa-minus"></i></span>
											<input type="text" name="quantity" value="1" size="2" id="input-quantity1" class="form-control qty" />
											<span id="add2" class="add"><i class="fa fa-plus"></i></span>
											<input type="hidden" name="product_id" value="1" />
										</p>
									</td>
									<td class="text-center">$70.00</td>
									<td class="text-center">
										<button type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</td>
								</tr>
								<tr>
									<td class="text-center">
										<a href="#">
											<img src="images/shop/thumb1.jpg" class="img-responsive" alt="thumb" title="thumb" />
											<div class="name">Cap
												<div class="rating">
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star-half-o" aria-hidden="true"></i>
												</div>
												<p>Color : <span>Grey</span></p>
											</div>
										</a>
									</td>
									<td class="text-center">$ 35.00</td>
									<td class="text-center">
										<p class="qtypara">
											<span class="minus"><i class="fa fa-minus"></i></span>
											<input type="text" name="quantity" value="1" size="2" id="input-quantity2" class="form-control qty" />
											<span class="add"><i class="fa fa-plus"></i></span>
											<input type="hidden" name="product_id" value="1" />
										</p>
									</td>
									<td class="text-center">$70.00</td>
									<td class="text-center">
										<button type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</td>
								</tr>
								<tr>
									<td class="text-center">
										<a href="#">
											<img src="images/shop/thumb1.jpg" class="img-responsive" alt="thumb" title="thumb" />
											<div class="name">Goggles
												<div class="rating">
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star-half-o" aria-hidden="true"></i>
												</div>
												<p>Color : <span>Black</span></p>
											</div>
										</a>
									</td>
									<td class="text-center">$ 35.00</td>
									<td class="text-center">
										<p class="qtypara">
											<span  class="minus"><i class="fa fa-minus"></i></span>
											<input type="text" name="quantity" value="1" size="2" id="input-quantity3" class="form-control qty" />
											<span  class="add"><i class="fa fa-plus"></i></span>
											<input type="hidden" name="product_id" value="1" />
										</p>
									</td>
									<td class="text-center">$70.00</td>
									<td class="text-center">
										<button type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
									</td>
								</tr>
								<tr>
									<td colspan="3" class="total">
										<p>Total Products Price (Tax Incl.)</p>
									</td>
									<td colspan="2" class="text-center price">
										<p>$ 140.00</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
				<div class="coupon pull-left">
					<div class="buttons">
							<a href="shop.html" class="btn btn-primary">Continue Shopping</a>
					</div>
				</div>
				<div class="pull-right">
					<div class="buttons">
						<a href="" class="btn btn-primary">Update Cart</a>
						<a href="checkout.html" class="btn btn-primary">Checkout</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- checkout end here -->

