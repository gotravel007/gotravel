<!--<?php
/* @var $this yii\web\View */
?>
<h1>/shop/index</h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>-->

<?php

use yii\helpers\Html;

?>

<div class="bread-crumb">
	<div class="container">
		<?php
		$this->title = 'Shop';
		?>
		<h1><?= Html::encode($this->title) ?></h1>
		<ul class="list-inline">
			<li><a href="index.html">home</a></li>
			<li><a href="shop.html">Shop</a></li>
		</ul>
	</div>
</div>
<!-- breadcrumb end here -->

<!-- bg start here -->
<div class="bg shopinner">
	<img src="images/shop/01.jpg" class="img-responsive" alt="bg" title="bg"  />
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="places">
					<h1>best quality travel products</h1>
					<hr>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>
					<ul class="list-inline">
						<li><a href="#">Shirts</a></li>
						<li><a href="#">Watches</a></li>
						<li><a href="#">Jeans</a></li>
						<li><a href="#">Camera</a></li>
						<li><a href="#">Bags</a></li>
						<li><a href="#">All items</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bg end here-->

<!-- shopping start here -->
<div class="shopping">
	<div class="container">
		<div class="shoppage">
			<div class="row sort">
				<div class="col-sm-2 list hidden-xs">
					<div class="btn-group btn-group-sm">
						<button type="button" id="gridview" class="btn btn-default active" data-toggle="tooltip" title="Grid"><i class="fa fa-th-large" aria-hidden="true"></i></button>
						<button  type="button" id="listview" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-list-ul" aria-hidden="true"></i></button>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group input-group input-group-sm">
						<label class="input-group-addon" for="input-sort">Sort By :    </label>
						<select id="input-sort" class="form-control selectpicker bs-select-hidden">
							<option value="" selected="selected">Default</option>
							<option value="">Name (A - Z)</option>
							<option value="">Name (Z - A)</option>
							<option value="">Price (Low &gt; High)</option>
							<option value="">Price (High &gt; Low)</option>
							<option value="">Rating (Highest)</option>
						</select>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group input-group input-group-sm">
						<label class="input-group-addon" for="input-sort">Show :    </label>
						<select id="input-name" class="form-control selectpicker bs-select-hidden">
							<option value="" selected="selected">20</option>
							<option value="">30</option>
							<option value="">40</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6 text-right">
					<ul class="pagination">
						<li>
							<a href="#" aria-label="Previous">Prev</a>
						</li>
						<li class="active">
							<a href="#"> 1</a> 
						</li>
						<li>
							<a href="#"> 2</a> 
						</li>
						<li>
							<a href="#">3</a>
						</li>
						<li>
							<a href="#"> 4</a> 
						</li>
						<li>
							<a href="#">...</a> 
						</li>
						<li>
							<a href="#">10</a> 
						</li>
						<li>
							<a href="#" aria-label="Next">Next</a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<a to="http://localhost/gotravel/frontend/web/index.php?r=site%2Fshopdetail">
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button></a>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Fancy tour bag</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Fancy watch</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<p>Sale</p>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Color goggles</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Digital camera</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Dark blue t-shirt</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">White & black hat</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Belts for men</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Tie linear blue</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Blue Flat shoe</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Travel bag</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<p>Sale</p>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Stylish blue jean</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
				<div class="product-layout product-grid col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<div class="product-thumb">
						<div class="image">
							<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
							<div class="hoverbox">
								<div class="buttons">
									<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
									<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
								</div>
							</div>
						</div>
						<div class="caption">
							<div class="rating">
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star" aria-hidden="true"></i>
								<i class="fa fa-star-half-o" aria-hidden="true"></i>
							</div>	
							<h4><a href="shopdetail.html">Women fashion top</a></h4>
							<p class="price">$241.99</p>
							<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquet velit a elit bibendum, congue gravida dui vulputate. Duis ex erat, malesuada eget turpis eget, placerat scelerisque nulla. Curabitur interdum facilisis arcu eget porta. In hac habitasse platea dictumst. Vestibulum nulla massa, pharetra quis metus eu, viverra faucibus enim.</p>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 text-left show">
				<p>Showing 1 to 12 of 19 (2 Pages)</p>
			</div>
			<div class="col-sm-6 text-right">
				<ul class="pagination">
					<li>
						<a href="#" aria-label="Previous">Prev</a>
					</li>
					<li class="active">
						<a href="#"> 1</a> 
					</li>
					<li>
						<a href="#"> 2</a> 
					</li>
					<li>
						<a href="#">3</a>
					</li>
					<li>
						<a href="#"> 4</a> 
					</li>
					<li>
						<a href="#">...</a> 
					</li>
					<li>
						<a href="#">10</a> 
					</li>
					<li>
						<a href="#" aria-label="Next">Next</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- shopping end here -->

<!-- news start here -->
<!--<div class="news">
	<img src="images/subscribe-bg.jpg" class="img-responsive sub" alt="subscribe-bg" title="subscribe-bg" />
	<div class="newsinner">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-xs-12">
				<div class="product-thumb">
					<div class="image">
						<a href="#"><img src="images/foot-banner1.png" alt="foot-banner1" title="foot-banner1" class="img-responsive" /></a>
						<div class="hoverbox"></div>
						<div class="matter">
							<p class="des"><span class="icon_building_alt"></span> Hotel</p>
							<button type="button"> Booking Now</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="product-thumb">
					<div class="image">
						<a href="#"><img src="images/foot-banner1.png" alt="foot-banner2" title="foot-banner2" class="img-responsive" /></a>
						<div class="hoverbox"></div>
						<div class="off">
							<span>Best Packages</span>
						</div>
						<div class="matter">
							<p class="des1">$1000 <span>Package</span> to <small>Dubai</small></p>
							<button type="button"> Booking Now</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="product-thumb">
					<div class="image">
						<a href="#"><img src="images/foot-banner1.png" alt="foot-banner3" title="foot-banner3" class="img-responsive" /></a>
						<div class="hoverbox"></div>
						<div class="matter">
							<p class="des2"><i class="icon_percent_alt"></i> 25% off<br> to <span>Island</span></p>
							<button type="button"> Booking Now</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12">
				<form class="subscribe">
					<div class="form-group">
						<div class="input-group">
							<label>Subscribe to our newsletter</label>
							<input placeholder="Enter your email" name="subscribe_email" value="" type="text">
							<button class="btn btn-default btn-lg" type="submit">Subscribe Now</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
</div>