<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>


<div class="bread-crumb">
	<div class="container">
		<h2>Login</h2>
		<ul class="list-inline">
			
			<li><?php
			$this->title = 'Login';
		 	$this->params['breadcrumbs'][] = $this->title;
			?></li>
		</ul>
	</div>
</div>

<div class="login">
	<div class="col-sm-6">
	<div class="leftside"></div></div>
	<div class="container">
		<div id="content" class="col-sm-12">
			<div class="row">
				<div class="col-sm-6">
					<div class="loginto">
						<h2>Login to Your account</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.</p>
						<div class="donot">Don't have account -
						<a href="register.html">register Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>	
				<div class="col-sm-6">
					<div class="loginnow">
						<div class="places">
							<h1>Login now</h1>
							<p>please login to your account</p>
							<hr>
						</div>
						<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
							<div class="form-group">
								<i class="fa fa-user"></i><?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
							</div>
							<div class="form-group">
								<i class="fa fa-key"></i><?= $form->field($model, 'password')->passwordInput() ?>
							</div>
							<div class="links">
								<?= $form->field($model, 'rememberMe')->checkbox() ?>
								Forgot Password?<?= Html::a('reset it', ['site/request-password-reset']) ?>.
							</div>
							<div class="form-group">
                    			<?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                			</div>
							<div class="or">
								<span>or</span>
								<hr/>
							</div>
							<ul class="list-unstyled">
								<li><a href="https://www.facebook.com/" target="_blank" class="fb"><i class="fa fa-facebook"></i> Login Via Facebook</a></li>
								<li><a href="https://www.twitter.com/" target="_blank" class="tw"><i class="fa fa-twitter"></i> Login Via Twitter</a></li>
								<li><a href="https://plus.google.com/" target="_blank" class="gp"><i class="social_googleplus"></i> Login Via Google+</a></li>
							</ul>
						<?php ActiveForm::end(); ?>
					</div>
				</div>			
			</div>
		</div>
	</div>
</div>
<!-- main container end here -->

