<?php

use yii\helpers\Html;

?>




<div class="bread-crumb">
	<div class="container">
		<?php
		$this->title = 'Shop Detail';
		?>
		<h1><?= Html::encode($this->title) ?></h1>
		<ul class="list-inline">
			<li><a href="index.html">home</a></li>
			<li><a href="shopdetail.html">Shop single</a></li>
		</ul>
	</div>
</div>
<!-- breadcrumb end here -->


<!-- shopping start here -->
<div class="shopping">
	<div class="container">
		<div class="row shopdetail">
			<!--thumb image code start-->
			<div class="col-sm-6 col-xs-12">
				<div class="col-sm-2">
					<ul class="thumbnails list-inline">	
						<li><a class="thumbnail" href="#"><img src="images/shop/small1.jpg" class="img-responsive" title="image" alt="image"></a></li>
			            <li><a class="thumbnail" href="#"><img src="images/shop/small1.jpg" class="img-responsive" title="image" alt="image"></a></li>
			            <li><a class="thumbnail" href="#"><img src="images/shop/small1.jpg" class="img-responsive" title="image" alt="image"></a></li>
			            <li><a class="thumbnail" href="#"><img src="images/shop/small1.jpg" class="img-responsive" title="image" alt="image"></a></li>
						<li><a class="thumbnail" href="#"><img src="images/shop/small1.jpg" class="img-responsive" title="image" alt="image"></a></li>
					</ul>
				</div>
				<div class="col-sm-10">
					<ul class="thumbnails list-inline">
					    <li class="image-additional"><a class="thumbnail" href="#"> <img src="images/shop/bigimage.jpg" class="img-responsive" title="image" alt="image"></a></li>
            		</ul>
				</div>
			</div>
			<!--thumb image code end-->
				
			<!--Product detail code start-->
			<div class="col-sm-6 col-xs-12">
				<h2>FANCY TOUR BAG</h2>
				<div class="rating">
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star-half-o" aria-hidden="true"></i>
				</div>
				<p class="price">$45.00</p>
				<ul class="list-unstyled links">
					<li>Product Code: <span>product 20</span></li>
                    <li>Available: <span>In Stock</span></li>
				</ul>
				<hr>
				<p class="shortdes">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat.</p>
				<div class="common">
					<p class="qtypara pull-left">
						<span id="minus2" class="minus"><i class="fa fa-minus"></i></span>
						<input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control qty" />
						<span id="add2" class="add"><i class="fa fa-plus"></i></span>
						<input type="hidden" name="product_id" value="1" />
					</p>
					<div class="buttons">
						<button type="button" class="btn-primary"><i class="fa fa-shopping-cart"></i>add to cart</button>
						<button type="button" class="btn-default"><i class="icon_heart"></i></button>
					</div>
				</div>
				<ul class="color list-inline">
					<li><a href="#"><div class="red"></div></a></li>
					<li><a href="#"><div class="green"></div></a></li>
					<li><a href="#"><div class="grey"></div></a></li>
					<li><a href="#"><div class="black"></div></a></li>
					<li><a href="#"><div class="orange"></div></a></li>
					<li><a href="#"><div class="blue"></div></a></li>
				</ul>
				<hr>
				<div class="share">
					<label>Share:</label>
					<ul class="list-inline">
						<li><a href="https://vimeo.com/" target="_blank"><i class="fa fa-vimeo"></i></a></li>
						<li><a href="https://twitter.com/" target="_blank"><i class="social_twitter"></i></a></li>
						<li><a href="https://in.linkedin.com/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="https://www.facebook.com/" target="_blank"><i class="social_facebook"></i></a></li>
						<li><a href="https://in.pinterest.com/" target="_blank"><i class="social_pinterest"></i></a></li>
						<li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
			</div>
			<!--Product detail code end-->
			<div class="col-sm-12 col-xs-12">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab-description" data-toggle="tab">description</a></li>
					<li><a href="#tab-information" data-toggle="tab">Information</a></li>
					<li><a href="#tab-review" data-toggle="tab">Reviews</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab-description">
						<p class="des">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.
						<br><br>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d. Lorem ipsum dolor sit amet.
						<br><br>
						sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d.  consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="tab-pane" id="tab-information">
						<p class="des">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<p class="des">sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum d.  consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="tab-pane" id="tab-review">
						<h2>2 reviews</h2>
						<div class="row">
							<div class="col-sm-12">
								<div class="image">
									<img src="images/tour/pic1.jpg" alt="user" title="user" class="img-responsive"/>
								</div>
								<div class="detail">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse.</p>
									<ul class="list-inline">
										<li>john doe - <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span></li>
										<li class="pull-right">April 10, 2017</li>
									</ul>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="image">
									<img src="images/tour/pic1.jpg" alt="user" title="user" class="img-responsive"/>
								</div>
								<div class="detail">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse.</p>
									<ul class="list-inline">
										<li>john doe - <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span></li>
										<li class="pull-right">April 10, 2017</li>
									</ul>
								</div>
							</div>
						</div>
						<form class="form-horizontal" id="form-review">
							<h2>Add review</h2>
							<label>Your Rating</label>
							<ul class="list-inline">
								<li><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span></li>
								<li><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span></li>
								<li><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span></li>
								<li><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span></li>
								<li><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span><span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span></li>
							</ul>
							<div class="form-group required">
								<div class="col-md-6 col-sm-12">
									<label class="control-label" for="input-name">Name</label>
									<input type="text" name="name" value="" id="input-name" class="form-control" />
								</div>
								<div class="col-md-6 col-sm-12">
									<label class="control-label" for="input-email">Email</label>
									<input type="text" name="email" value="" id="input-email" class="form-control" />
								</div>
							</div>
							<div class="form-group required">
								<div class="col-sm-12">
									<label class="control-label" for="input-review">Message</label>
									<textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
								</div>
							</div>
							<div class="buttons clearfix">
								<div class="pull-right">
									<button type="submit" id="button-review" class="btn btn-primary">Send</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--<div class="row related">-->
		<div class="">
			<div class="places">
				<h1>related tours</h1>
				<hr>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="product-thumb">
					<div class="image">
						<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
						<div class="hoverbox">
							<div class="buttons">
								<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
							</div>
						</div>
					</div>
					<div class="caption">
						<div class="rating">
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star-half-o" aria-hidden="true"></i>
						</div>	
						<h4><a href="shopdetail.html">Dark blue t-shirt</a></h4>
						<p class="price">$241.99</p>							
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="product-thumb">
					<div class="image">
						<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
						<div class="hoverbox">
							<div class="buttons">
								<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
							</div>
						</div>
					</div>
					<div class="caption">
						<div class="rating">
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star-half-o" aria-hidden="true"></i>
						</div>	
						<h4><a href="shopdetail.html">White & black hat</a></h4>
						<p class="price">$241.99</p>							
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="product-thumb">
					<div class="image">
						<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
						<div class="hoverbox">
							<div class="buttons">
								<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
							</div>
						</div>
					</div>
					<div class="caption">
						<div class="rating">
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star-half-o" aria-hidden="true"></i>
						</div>	
						<h4><a href="shopdetail.html">Belts for men</a></h4>
						<p class="price">$241.99</p>							
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="product-thumb">
					<div class="image">
						<a href="shopdetail.html"><img src="images/shop/02.jpg" alt="image" title="image" class="img-responsive" /></a>
						<div class="hoverbox">
							<div class="buttons">
								<button type="button" class="btn-danger"><i class="fa fa-shopping-cart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-heart"></i></button>
								<button type="button" class="btn-danger"><i class="fa fa-eye"></i></button>
							</div>
						</div>
					</div>
					<div class="caption">
						<div class="rating">
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star" aria-hidden="true"></i>
							<i class="fa fa-star-half-o" aria-hidden="true"></i>
						</div>	
						<h4><a href="shopdetail.html">Tie linear blue</a></h4>
						<p class="price">$241.99</p>							
					</div>
				</div>
			</div>
		</div>			
		
	</div>
</div>
<!-- shopping end here -->