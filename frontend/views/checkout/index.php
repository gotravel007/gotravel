<!--<?php
/* @var $this yii\web\View */
?>
<h1>/checkout/index</h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>
-->

<!-- breadcrumb start here -->
<div class="bread-crumb">
	<div class="container">
		<h2>Checkout</h2>
		<ul class="list-inline">
			<li><a href="index.html">home</a></li>
			<li><a href="shop.html">My account</a></li>
			<li><a href="checkout.html">checkout</a></li>
		</ul>
	</div>
</div>
<!-- breadcrumb end here -->

<!-- checkout start here -->
	<div class="checkout">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="places">
						<h1>Billing Address</h1>
						<hr>
					</div>
					<form method="post" enctype="multipart/form-data">	
						<fieldset>	
							<div class="form-group required row">
								<div class="col-sm-6">
									<label>First Name*</label>
									<input name="firstname" value="" placeholder="" id="input-firstname" class="form-control" type="text">
								</div>
								<div class="col-sm-6">
									<label>Last Name*</label>
									<input name="lastname" value="" placeholder="" id="input-lastname" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group required row">
								<div class="col-sm-6">
									<label>Address*</label>
									<input name="address" value="" placeholder="" id="input-address" class="form-control" type="text">
								</div>
								<div class="col-sm-6">
									<label>Town / City*</label>
									<input name="town" value="" placeholder="" id="input-town" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group required row">
								<div class="col-sm-4">
									<label>Postcode / Zip*</label>
									<input name="postcode" value="" placeholder="" id="input-postcode" class="form-control" type="text">
								</div>
								<div class="col-sm-4">
									<label>Email Address*</label>
									<input name="emailaddress" value="" placeholder="" id="input-emailaddress" class="form-control" type="text">
								</div>
								<div class="col-sm-4">
									<label>Phone Nunmber*</label>
									<input name="phone" value="" placeholder="Phone Number" id="input-phone" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="check">
									<input type="checkbox" value="None"  name="pool" class="checkclass checkbox-inline"/>
									Create Account? <span>You will receive email with temporary generated password with after login tou need to change.</span>
								</label>
							</div>
						</fieldset>
					</form>
				</div>
				
				<div class="col-sm-12 col-xs-12 mycart">
					<div class="places">
						<h1>Your Products</h1>
						<hr>
					</div>
					<form method="post" enctype="multipart/form-data">
						<div class="table-responsive">
							<table class="table tabl1">
								<tbody>
									<tr>
										<td class="text-center">
											<a href="#">
												<img src="images/shop/thumb1.jpg" class="img-responsive" alt="thumb" title="thumb" />
												<div class="name">Travel Bag
													<div class="rating">
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star-half-o" aria-hidden="true"></i>
													</div>
													<p>Color : <span>Black</span></p>
												</div>
											</a>
										</td>
										<td class="text-right">$70.00</td>
									</tr>
									<tr>
										<td class="text-center">
											<a href="#">
												<img src="images/shop/thumb1.jpg" class="img-responsive" alt="thumb" title="thumb" />
												<div class="name">Cap
													<div class="rating">
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star-half-o" aria-hidden="true"></i>
													</div>
													<p>Color : <span>Grey</span></p>
												</div>
											</a>
										</td>
										<td class="text-right">$70.00</td>
									</tr>
									<tr>
										<td colspan="1"></td>
										<td colspan="1">
											<p class="text-left">Cart Subtotal<span class="pull-right">$ 140.00</span></p>
											<p class="text-left">Shipping<span class="pull-right">Free shipping</span></p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
				</div>
				
				<div class="col-sm-12 col-xs-12 bank-transfer">
					<form method="post" enctype="multipart/form-data">	
						<fieldset>	
							<div class="form-group">
								<label class="check">
									<input type="checkbox" value="None"  name="pool" class="checkclass checkbox-inline"/>
									Direct Bank Transfer
								</label>
							</div>
							<div class="form-group">
								<label class="check">
									<input type="checkbox" value="None"  name="pool" class="checkclass checkbox-inline"/>
									Paypal  System
								</label>
							</div>
							<div class="pull-right">
								<button type="submit" class="btn-primary btn-block">Place Order</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
<!-- checkout end here -->