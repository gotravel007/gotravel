<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900%7CPT+Serif:400,400i,700,700i" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- top start here -->

    <div id="top">
        <div class="container">
            <div id="top-links" class="nav">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="#">Welcome to TMD travel agency</a>
                    </li>
                    <li>
                        <form method="post" enctype="multipart/form-data" id="form-language">
                            <div class="btn-group">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    Language : <span class="name"><span class="hidden-xs">English</span> <i class="fa fa-caret-down"></i></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:;">English</a></li>
                                </ul>
                            </div>
                        </form>
                    </li>
                    <li>
                        <form method="post" enctype="multipart/form-data" id="form-currency">
                            <div class="btn-group">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                      Currency : <span class="name"><span class="hidden-xs"> (USD)</span> <i class="fa fa-caret-down"></i></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><button class="currency-select btn btn-link btn-block" type="button" name="EUR">&#8364; Euro</button></li>
                                    <li><button class="currency-select btn btn-link btn-block" type="button" name="GBP">&#163; Pound Sterling</button></li>
                                    <li><button class="currency-select btn btn-link btn-block" type="button" name="USD">$ US Dollar</button></li>
                                </ul>
                            </div>
                        </form>
                    </li>
                </ul>
                
                <ul class="list-inline pull-right button">
                    <li><a href="login.html">Login</a>
                        <a href="register.html">Register</a>
                    </li>
                    <li><a href="my_account_wishlist.html"><i class="icon_heart"></i>Wishlist</a></li>
                    <li><a href="shoppingcart.html"><i class="icon_cart"></i>Cart</a></li>  
                </ul>
            </div>
        </div>
    </div>
<!-- top end here -->
    
<!-- header start here-->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-xs-12">
                    <div class="social-icon">
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/" target="_blank"><i class="social_googleplus" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://in.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="https://photos.google.com/" target="_blank"><i class="social_picassa"></i></a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-sm-4 col-md-4 col-xs-12">
                    <div id="logo">
                        <a href="index.html"><img class="img-responsive" src="images/logo.png" alt="logo" title="logo" /></a>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-xs-12">
                    <div class="button-login pull-right">
                        <button type="button" class="btn btn-default btn-lg" onclick="location.href='tour-booking-form.html'">Booking Now</button>
                        <button type="button" class="btn btn-primary btn-lg" onclick="location.href='tour-grid-view.html'">Take a tour <i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </header>
<!-- header end here -->

<!-- menu start here -->
    <div id="menu"> 
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <nav class="navbar">
                        <div class="navbar-header">
                            <span class="menutext visible-xs">Menu</span>
                            <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="btn btn-navbar navbar-toggle" type="button"><i class="fa fa-bars" aria-hidden="true"></i></button>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex1-collapse padd0">
                            <ul class="nav navbar-nav">
                                <li class="dropdown"><a href="http://localhost/gotravel/frontend/web/index.php" class="dropdown-toggle" data-toggle="dropdown">HOME</a>
                                    
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Travel</a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-submenu">
                                            <a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Tour</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="tour-grid-view.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Tour</a></li>
                                                <li><a href="tour-detail-view.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Tour Detail</a></li>
                                                <li><a href="tour-booking-form.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Tour Booking Form</a></li>
                                                <li><a href="thank-you.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Thank You</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-submenu">
                                            <a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Places</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="place.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Places</a></li>
                                                <li><a href="place-detail-view.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Places Detail</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-submenu">
                                            <a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Flight</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="flights.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Flight</a></li>
                                                <li><a href="flight-detail-view.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Flight Detail</a></li>
                                                <li><a href="flight-booking-form.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Flight Booking Form</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-submenu">
                                            <a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Hotel</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="hotel.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Hotel</a></li>
                                                <li><a href="hotel-detail-view.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Hotel Detail</a></li>
                                                <li><a href="hotel-booking-form.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Hotel Booking Form</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Packages</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="packages.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Packages</a></li>
                                        <li><a href="package-detail-view.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Packages Detail</a></li>
                                    </ul>
                                </li>
                                <li><a href="guides.html">GUIDES</a></li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="http://localhost/gotravel/frontend/web/shop"><i class="fa fa-angle-right" aria-hidden="true"></i>Shop</a></li>
                                        <li><a href="http://localhost/gotravel/frontend/web/shopdetail"><i class="fa fa-angle-right" aria-hidden="true"></i>Product Detail</a></li>
                                        <li><a href="http://localhost/gotravel/frontend/web/shopcart"><i class="fa fa-angle-right" aria-hidden="true"></i>Shopping Cart</a></li>
                                        <li><a href="http://localhost/gotravel/frontend/web/checkout"><i class="fa fa-angle-right" aria-hidden="true"></i>Checkout</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">user</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="login.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Login</a></li>
                                        <li><a href="register.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Register</a></li>
                                        <li><a href="my_account_dashboard.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Dashboard</a></li>
                                        <li><a href="my_account_profile.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Profile</a></li>
                                        <li><a href="my_account_profile_edit.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Profile Edit</a></li>
                                        <li><a href="my_account_booking.html"><i class="fa fa-angle-right" aria-hidden="true"></i>My Boooking List</a></li>
                                        <li><a href="my_account_wishlist.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Wishlist</a></li>
                                        <li><a href="my_account_setting.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Setting</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">BLOG</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="blog.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Blog</a></li>
                                        <li><a href="blog-detail.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Post Page</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">PAGES</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="gallery.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Gallery</a></li>
                                        <li><a href="error-404.html"><i class="fa fa-angle-right" aria-hidden="true"></i>404 error Page</a></li>
                                        <li><a href="faq.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Faq</a></li>
                                        <li><a href="about.html"><i class="fa fa-angle-right" aria-hidden="true"></i>About</a></li>
                                        <li><a href="contact.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Contact</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
<!-- menu end here -->

        <?= $content ?>



<!-- news start here -->
<div class="news">
    <img src="images/subscribe-bg.jpg" class="img-responsive sub" alt="subscribe-bg" title="subscribe-bg" />
    <div class="newsinner">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <div class="product-thumb">
                    <div class="image">
                        <a href="#"><img src="images/foot-banner1.png" alt="foot-banner1" title="foot-banner1" class="img-responsive" /></a>
                        <div class="hoverbox"></div>
                        <div class="matter">
                            <p class="des"><span class="icon_building_alt"></span> Hotel</p>
                            <button type="button"> Booking Now</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="product-thumb">
                    <div class="image">
                        <a href="#"><img src="images/foot-banner1.png" alt="foot-banner2" title="foot-banner2" class="img-responsive" /></a>
                        <div class="hoverbox"></div>
                        <div class="off">
                            <span>Best Packages</span>
                        </div>
                        <div class="matter">
                            <p class="des1">$1000 <span>Package</span> to <small>Dubai</small></p>
                            <button type="button"> Booking Now</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="product-thumb">
                    <div class="image">
                        <a href="#"><img src="images/foot-banner1.png" alt="foot-banner3" title="foot-banner3" class="img-responsive" /></a>
                        <div class="hoverbox"></div>
                        <div class="matter">
                            <p class="des2"><i class="icon_percent_alt"></i> 25% off<br> to <span>Island</span></p>
                            <button type="button"> Booking Now</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <form class="subscribe">
                    <div class="form-group">
                        <div class="input-group">
                            <label>Subscribe to our newsletter</label>
                            <input placeholder="Enter your email" name="subscribe_email" value="" type="text">
                            <button class="btn btn-default btn-lg" type="submit">Subscribe Now</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- news end here -->

<footer>
    

    <div class="container">
        <div class="row padd-b">
            <div class="col-sm-3">
                <img src="images/foot-logo.png" class="img-responsive" alt="foot-logo" title="foot-logo" />
                <p class="des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam blandit condimen-tum tortor, eget ultricies ante luctus sed. Praesent lobortis scelerisque ipsum sed commodo[...]</p>
            </div>
            <div class="col-sm-3 contact">
                <h3>Contact us</h3>
                <ul class="list-inline">
                    <li>
                        <div class="inner"><i class="fa fa-home" aria-hidden="true"></i> Address</div>
                        <div class="in"><a href="#"> : 123 Lorem  ipsum dolor, Ludhiana, India</a></div>
                    </li>
                    <li>
                        <div class="inner"><i class="fa fa-phone" aria-hidden="true"></i>Phone No.</div>
                        <div class="in"><a href="#"> : (+91)123457890, (1234)</a></div>
                    </li>
                    <li>
                        <div class="inner"><i class="fa fa-envelope" aria-hidden="true"></i>Email</div>
                        <div class="in"><a href="#"> : 1234@example.com</a></div>
                        <br>
                        <div class="inner paddleft">website</div>
                        <div class="in"><a href="#">  : www.Domain.com</a></div>
                    </li>
                </ul>
            </div>
            <div class="col-sm-3 info">
                <h3>Information</h3>
                <ul class="list-inline">
                    <li><a href="index.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Home</a></li>
                    <li><a href="login.html"><i class="fa fa-angle-right" aria-hidden="true"></i>My Account</a></li>
                    <li><a href="about.html"><i class="fa fa-angle-right" aria-hidden="true"></i>About</a></li>
                    <li><a href="packages.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Packages</a></li>
                    <li><a href="gallery.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Gallery</a></li>
                    <li><a href="tour-grid-view.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Best Tour</a></li>
                    <li><a href="hotel.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Hotels</a></li>
                    <li><a href="place.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Best Places</a></li>
                    <li><a href="blog.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Blog</a></li>
                </ul>
            </div>
            <div class="col-sm-3 insta">
                <h3>Instagram</h3>
                <ul class="list-inline">
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins1" alt="ins1" /></li>
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins2" alt="ins2" /></li>
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins3" alt="ins3" /></li>
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins4" alt="ins4" /></li>
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins5" alt="ins5" /></li>
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins6" alt="ins6" /></li>
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins7" alt="ins7" /></li>
                    <li><img src="images/ins1.jpg" class="img-responsive" title="ins8" alt="ins8" /></li>
                </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="powered">
                    <div class="col-sm-6 padd0">
                        <p>© Copyright 2017. <span>TMD Travel Agency </span> by <a target="_blank" href="https://www.themultimediadesigner.com/">The Multimedia Designer</a> </p>
                    </div>
                    <div class="col-sm-6 padd0 text-right">
                        <div class="social-icon">
                            <ul class="list-inline">
                                <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://plus.google.com/" target="_blank"><i class="social_googleplus" aria-hidden="true"></i></a></li>
                                <li><a href="https://in.linkedin.com/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="https://in.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="https://photos.google.com/" target="_blank"><i class="social_picassa"></i></a></li>
                                <li><a href="https://www.youtube.com/?gl=IN" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
