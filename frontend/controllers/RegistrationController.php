<?php

namespace frontend\controllers;
use frontend\models\Registration;
use yii\web\Controller;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;


class RegistrationController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');

        
    }

    public function actionSignup()
    {
        $model = new Registration();
        $model->load(Yii::$app->request->post());
            
        

        return $this->render('registration', [
            'model' => $model,
        ]);
    }

}
