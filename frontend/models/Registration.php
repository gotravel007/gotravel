<?php


namespace frontend\models;

use yii\base\Model;
use common\models\User;

use Yii;

/**
 * This is the model class for table "registration".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $address
 * @property int $status
 * @property int $usertype
 * @property int $contactno
 * @property string $email
 * @property string $password_hash
 */
class Registration extends \yii\db\ActiveRecord
{


    public $email;
    public $status;
    public $password;
    public $firstname;
    public $lastname;
    public $address;
    public $usertype;
    public $contactno;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'address', 'contactno', 'email', 'password_hash'], 'required'],
            [['status', 'usertype', 'contactno'], 'integer'],
            [['firstname', 'lastname', 'address', 'email', 'password_hash'], 'string', 'max' => 255],
            [['address'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'address' => 'Address',
            'status' => 'Status',
            'usertype' => 'Usertype',
            'contactno' => 'Contactno',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
        ];
    }


     public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->firstname = $this->firstname;
        $user->lastname = $this->lastname;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->address = $this->address;
        $user->usertype = $this->usertype;
        $user->contactno = $this->contactno;
        
        
        
        return $user->save() ? $user : null;
    }
}
