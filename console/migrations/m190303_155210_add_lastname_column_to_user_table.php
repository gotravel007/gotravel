<?php

use yii\db\Migration;

/**
 * Handles adding lastname to table `user`.
 */
class m190303_155210_add_lastname_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'lastname', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'lastname');
    }
}
