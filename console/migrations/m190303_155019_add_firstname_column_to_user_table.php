<?php

use yii\db\Migration;

/**
 * Handles adding firstname to table `user`.
 */
class m190303_155019_add_firstname_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'firstname', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'firstname');
    }
}
