<?php

use yii\db\Migration;

/**
 * Handles adding address to table `user`.
 */
class m190303_161521_add_address_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'address', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'address');
    }
}
