<?php

use yii\db\Migration;

/**
 * Class m190303_032408_users_table
 */
class m190303_032408_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
  /*  public function safeUp()
    {
        $this->createTable('{{%user_table}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull()->unique(),
            'lastname' => $this->string()->notNull()->unique(),
            'address' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'usertype' => $this->smallInteger()->notNull()->defaultValue(10),
            'contactno' => $this->integer()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            
            
            
        ]);

    }

    /**
     * {@inheritdoc}
     */
    /*public function safeDown()
    {
        echo "m190303_032408_users_table cannot be reverted.\n";

        return false;
    }*/

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('{{%user_table}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull()->unique(),
            'lastname' => $this->string()->notNull()->unique(),
            'address' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'usertype' => $this->smallInteger()->notNull()->defaultValue(10),
            'contactno' => $this->integer()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            
            
            
        ]);
    }

    public function down()
    {
         $this->dropTable('user_table');
    }
    
}
