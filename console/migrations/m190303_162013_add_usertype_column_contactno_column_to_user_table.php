<?php

use yii\db\Migration;

/**
 * Handles adding usertype_column_contactno to table `user`.
 */
class m190303_162013_add_usertype_column_contactno_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'usertype', $this->string());
        $this->addColumn('user', 'contactno', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'usertype');
        $this->dropColumn('user', 'contactno');
    }
}
